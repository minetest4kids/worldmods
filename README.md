# Mods für unsere Server/Projekte

[TOC]

## Creative
[Creative Mods](https://codeberg.org/minetest4kids/worldmods/src/branch/creative)

```bash
git clone --branch=creative https://codeberg.org/minetest4kids/worldmods.git
```

Die Submodules müssen noch initialisiert werden.

```bash
git submodule update --init --recursive
```


## Survival
[Survival Mods](https://codeberg.org/minetest4kids/worldmods/src/branch/survival)

```bash
git clone --branch=survival https://codeberg.org/minetest4kids/worldmods.git
```

Die Submodules müssen noch initialisiert werden.

```bash
git submodule update --init --recursive
```

## FAS
[FAS Mods](https://codeberg.org/minetest4kids/worldmods/src/branch/fas)

```bash
git clone --branch=fas https://codeberg.org/minetest4kids/worldmods.git
```

Die Submodules müssen noch initialisiert werden.

```bash
git submodule update --init --recursive
```

## Reunion
[Reunion Mods](https://codeberg.org/minetest4kids/worldmods/src/branch/reunion)

```bash
git clone --branch=reunion https://codeberg.org/minetest4kids/worldmods.git
```

Die Submodules müssen noch initialisiert werden.

```bash
git submodule update --init --recursive
```

